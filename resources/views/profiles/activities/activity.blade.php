<div class="card" style="margin-bottom: 25px">
        <span class="flex">
            <div class="card-header">
                 {{ $header }}
            </div>

        </span>


    <div class="card-body">

            {{ $body }}

    </div>

</div>