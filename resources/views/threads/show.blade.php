@extends('layouts.app')

@section('content')
    <thread-view :initial-replies-count="{{ $thread->replies_count }}" inline-template>

        <div class="container">
            <div class="col-md-4" style="float: right;" >
                <div class="card">
                    <div class="card-body">
                        <p>
                            This thread was published {{ $thread->created_at->diffForHumans() }} <br> by
                            <a href="{{ route('profile', $thread->creator) }}"> {{ $thread->creator->name }}</a>, and currently
                            has <span v-text="repliesCount"></span> {{ str_plural('comment', $thread->replies_count) }}.
                        </p>

                        <p>
                        <subscribe-button :active="{{ json_encode($thread->isSubscribedTo) }}"></subscribe-button>
                        </p>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="level">
                            <span class="flex">
                               <a href="{{ route('profile', $thread->creator) }}">{{ $thread->creator->name }}</a> posted:
                                <div class="card-header">
                                    {{$thread->title}}
                                </div>
                            </span>


                            @can ('update', $thread)
                                <form action="{{ $thread->path() }}" method="POST">

                                {{csrf_field()}}
                                {{method_field('DELETE')}}

                                <button type="submit" class="btn btn-link" style="background-color: #f9f9f9; margin-top: 23px; padding-top: 11px; padding-bottom: 12px; border-bottom:1px solid rgba(0, 0, 0, 0.125); border-radius:0 ">Delete Thread</button>

                                </form>
                            @endcan
                        </div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                         <div class="body">{{ $thread->body }}</div>
                        </div>
                    </div>
                        <replies @added="repliesCount++" @removed="repliesCount--"></replies>

                    {{--</div>--}}

                </div>

            </div>
        </div>
    </thread-view>
@endsection
