<reply :attributes="{{ $reply }}" inline-template v-cloak>
    <div id="reply-{{ $reply->id }}" class="card" >
        <div class="card-header">
            <div class="level">
                <h6 class="flex">
                    <a href="{{ route('profile', $reply->owner) }}"> {{ $reply->owner->name }}
                    </a> said {{$reply->created_at->diffForHumans()}}...
                </h6>

                @if (Auth::check())
                    <div>
                        <favorite :reply="{{ $reply }}"></favorite>
                    </div>
                @endif
            </div>
        </div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif


            <div class="body">
                <div v-if="editing">
                    <div class="form-group">
                        <textarea class="form-control" v-model="body"></textarea>

                    </div>
                    <button class="bt n btn-primary" @click="update">Update</button>
                    <button class="btn btn-link" @click="editing = false">Cancel</button>
                </div>

                <div v-else v-text="body"></div>
            </div>

       @can ('update', $reply)
            <div class="panel-footer level" style="margin-top: 54px;">
                <button class="btn mr-1" style="background-color: #ccc" @click="editing = true">Edit</button>
                <button class="btn mr-1 btn-danger" @click="destroy">Delete</button>
            </div>
        @endcan
        </div>
    </div>
</reply>