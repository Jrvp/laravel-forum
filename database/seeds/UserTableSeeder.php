<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // How many threads you need, defaulting to 10
        $count = (int)$this->command->ask('How many threads do you need ?', 10);

        $this->command->info("Creating {$count} threads.");

        // Create the Threads
        $threads = factory(App\Thread::class, $count)->create();

        $this->command->info('Threads Created!');

        foreach ($threads as $thread) {
            factory(App\Reply::class, 10)->create([
                'thread_id' => $thread->id,
            ]);
        }
    }
}
