### Setup Nginx image
FROM nginx:1.14-alpine AS nginx

### Base PHP container ####
FROM php:7.3-fpm-alpine AS base
WORKDIR /app

RUN apk update && \
	apk add --no-cache \
		supervisor && \
	docker-php-ext-install pdo && \
	docker-php-ext-install pdo_mysql && \
	docker-php-ext-install mbstring

# Prepare supervisor
RUN sed -i 's/^\(\[supervisord\]\)$/\1\nnodaemon=true/' /etc/supervisord.conf

# Prepare Nginx
RUN addgroup -S nginx && \
	adduser -D -S -h /var/cache/nginx -s /sbin/nologin -G nginx nginx && \
	mkdir -p /var/log/nginx && \
	ln -sf /dev/stdout /var/log/nginx/access.log && \
	ln -sf /dev/stderr /var/log/nginx/error.log
COPY --from=nginx /usr/sbin/nginx /usr/sbin/nginx
COPY --from=nginx /usr/sbin/nginx-debug /usr/sbin/nginx-debug
COPY --from=nginx /usr/lib/* /usr/lib/
COPY --from=nginx /var/cache/nginx /var/cache/nginx
RUN rm -rf /etc/nginx && \
    mkdir /etc/nginx && \
    wget -qO- https://github.com/h5bp/server-configs-nginx/archive/2.0.0.tar.gz | \
    tar xvz -C /etc/nginx --strip-components 1 && \
    mkdir -p /etc/nginx/logs && \
    sed -i -e 's/user www www;/user  nginx;/g' /etc/nginx/nginx.conf
COPY fastcgi_params /etc/nginx/fastcgi_params
COPY nginx.conf /etc/nginx/sites-enabled/site.conf
### --- ####

### PHP Composer container ####
FROM base AS vendor

RUN	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer

ADD composer.lock /app/composer.lock
ADD composer.json /app/composer.json
ADD database /app/database

RUN composer install --no-ansi --no-interaction --no-progress --optimize-autoloader --no-scripts
### --- ####

### Release container ####
FROM base AS release

ENV LOG_CHANNEL stderr
ENV APP_KEY base64:vnXoVW98T4zqLsQxwrS0qWmyrX95w3sU/Bn7LnlrxjQ=

# Prepare Laravel app file storage
RUN mkdir -p \
    /storage/app/public \
    /storage/framework/cache \
    /storage/framework/sessions \
    /storage/framework/testing \
    /storage/framework/views \
    /storage/logs

# Set supervisor conf
COPY supervisor.conf /etc/supervisor.d/supervisor.ini

# Copy all application code
COPY --chown=www-data:www-data . /app

# Create nginx webroot and storage symlinks
RUN mv .env.example .env && \
    rm -rf /var/www/html && \
    ln -s /app/public /var/www/html && \
    rm -rf /app/storage && \
    ln -s /storage /app/storage && \
    chown -R www-data:www-data /storage

# Copy built dependencies into release
COPY --chown=www-data:www-data --from=vendor /app/vendor /app/vendor

CMD ["supervisord", "-c", "/etc/supervisord.conf"]
### --- ####