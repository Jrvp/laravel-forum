<?php
/**
 * Created by PhpStorm.
 * User: optimist
 * Date: 2019-02-06
 * Time: 16:15
 */

namespace App\Filters;


use Illuminate\Http\Request;

abstract class Filters
{

    protected $request, $builder;

    protected $filters = [];


    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function apply($builder)
    {
        $this->builder = $builder;

        foreach ($this->getFilters() as $filter => $value){
            if (method_exists($this, $filter)){
                $this->$filter($value);
            }
        }

        return $this->builder;
    }


    public function getFilters(){

        return $this->request->only($this->filters);
    }
}