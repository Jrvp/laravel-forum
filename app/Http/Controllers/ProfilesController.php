<?php

namespace App\Http\Controllers;

use App\Activity;
use App\User;
use Illuminate\Http\Request;

class ProfilesController extends Controller
{
    public function show(User $user){

        $activityFeed = Activity::feed($user);

/*        foreach($activityFeed as $day) {
            foreach($day as $activity) {
                dump($activity);
                dump($activity->subject);
            }
        }
        dd();*/

        return view('profiles.show', [
            'profileUser' => $user,
            'activities' => Activity::feed($user)
        ]);
    }


}
